<h1>Overview</h1>

<p>2D Platformer with SFML.
Currently a Work in Progress.</p>
<p>Pull Requests, suggestions, bug/ issue reports
are all welcomed!</p>

<h3>Dependencies</h3>
<ul>
  <li>SFML</li>
</ul> 


<h1>License</h1>
See LICENSE.md for the Code's License.