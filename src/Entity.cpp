/*
 * Entity.cpp
 *
 *  Created on: Jan 21, 2014
 */

#include "WindowManager.hpp"
#include "World.hpp"
#include "Entity.hpp"
#include "Location.hpp"
#include "ResourceManager.hpp"

Entity::Entity(EntityType eType, Location loc, unsigned short id) :
		DrawableEntity(eType, loc), m_id(id)
{
}

Entity::~Entity()
{
}

unsigned short Entity::getID()
{
	return m_id;
}

void Entity::update()
{
}

void Entity::draw()
{
	DrawableEntity::draw();
}

