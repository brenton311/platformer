/*
 * World.cpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#include "World.hpp"
#include "Chunk.hpp"
#include "Location.hpp"
#include "EntityType.hpp"
#include "WindowManager.hpp"
#include "Exceptions.hpp"
#include <iostream>
#include <algorithm>



// The first id in the world (0) is the Local Players ID
World::World(const std::string& name) :
		m_nextID(0), m_name(name)
{
}

World::~World()
{
	for (Chunk* c : m_loadedChunks)
		delete c;
}

void World::loadChunk(int loc)
{
	for (Chunk* c : m_loadedChunks)
	{
		if (c->getLocation() == loc)
			throw Exception("Trying to load Already Loaded Chunk!");
	}

	Chunk* c = new Chunk(loc, "saves/" + m_name);
	c->load();
	m_loadedChunks.push_back(c);
}

void World::spawnEntity(EntityType eType, Location loc)
{
	getChunk(loc.getChunkLoc())->spawnEntity(eType, loc, genEntityID());
}

// All entitys are organized by there ID's
unsigned short World::genEntityID()
{
	return m_nextID++;
}

const std::string& World::getName()
{
	return m_name;
}

void World::update()
{
	for (Chunk* c : m_loadedChunks)
		c->update();
}

void World::unloadChunk(int loc)
{
	for (unsigned int i = 0; i < m_loadedChunks.size(); i++)
	{
		if (m_loadedChunks[i]->getLocation() == loc)
		{
			// Free the memory then swap the chunk to the end and pop it out
			delete m_loadedChunks[i];
			std::swap(m_loadedChunks[i],
					m_loadedChunks[m_loadedChunks.size() - 1]);
			m_loadedChunks.pop_back();
			return;
		}
	}
	throw Exception("Failed to Unload Unknown Chunk!");
}

Chunk* World::getChunk(int loc)
{
	for (Chunk* c : m_loadedChunks)
	{
		if (c->getLocation() == loc)
			return c;
	}

	throw Exception("A Unloaded Chunk Was Requested!");
}

void World::draw()
{
	for (Chunk* c : m_loadedChunks)
		c->draw();
}
