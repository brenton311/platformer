/*
 * TileUtl.cpp
 *
 *  Created on: Jan 19, 2014
 *
 */

#include "TileUtl.hpp"
#include "Exceptions.hpp"


bool TileUtl::isBackTile(TileID id)
{
	if(id < TileID::FrontWood)
		return true;
	return false;
}

TileID TileUtl::parseID(unsigned short id)
{
	switch (id)
	{
	case TileID::Air:
		return TileID::Air;
	case TileID::Stone:
		return TileID::Stone;
	case TileID::Grass:
		return TileID::Grass;
	case TileID::Dirt:
		return TileID::Dirt;
	case TileID::Wood:
		return TileID::Wood;
	case TileID::FrontWood:
		return TileID::FrontWood;

	default:
		throw Exception("Requested a Tile with a unknown ID!");
	}
}

