/*
 * DrawableEntity.cpp
 *
 *  Created on: Jan 23, 2014
 */

#include "DrawableEntity.hpp"
#include "WindowManager.hpp"
#include "ResourceManager.hpp"

DrawableEntity::DrawableEntity(EntityType eType, Location loc) :
		type(eType), m_loc(loc)
{
	m_sprite.setPosition(loc.getLoc());

	switch (eType)
	{
	case EntityType::Player:
		m_sprite.setTexture(
				ResourceManager::getTexture(TextureID::PlayerSprite));
		break;
	case EntityType::LocalPlayer:
		m_sprite.setTexture(
				ResourceManager::getTexture(TextureID::PlayerSprite));
		break;
	}

	m_sprite.setTextureRect(sf::IntRect(32.0f, 0.0f, 32.0f, 32.0f));
}

void DrawableEntity::draw()
{
	WindowManager::getWindow().draw(m_sprite);
}

