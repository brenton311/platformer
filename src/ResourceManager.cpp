/*
 * ResourceManager.cpp
 *
 *  Created on: Jan 22, 2014
 */

#include <string>
#include <iostream>
#include "ResourceManager.hpp"
#include "Exceptions.hpp"

std::map<TextureID, sf::Texture*> ResourceManager::s_resources;

void ResourceManager::loadTexture(const std::string& path, TextureID id)
{
	sf::Texture* texture = new sf::Texture;
	texture->setRepeated(true);
	if (!texture->loadFromFile(path))
	{
		delete texture;
		throw Exception("Failed to load Resource: " + path + "!");
	}
	s_resources[id] = texture;
}

// Initilize all of the resources
void ResourceManager::init()
{
	try
	{
		loadTexture("assets/player.png", TextureID::PlayerSprite);
		loadTexture("assets/terrain/stone.png", TextureID::StoneTile);
		loadTexture("assets/terrain/grass.png", TextureID::GrassTile);
	}
	catch (Exception& e)
	{
		std::cout << "Resource Loading Error: " + e.getErrorMsg() << std::endl;
	}
}

sf::Texture& ResourceManager::getTexture(TextureID id)
{
	return *s_resources[id];
}
