/*
 * GameManager.cpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#include <SFML/Graphics.hpp>
#include "GameManager.hpp"
#include "WindowManager.hpp"
#include "World.hpp"
#include "Entity.hpp"
#include "ResourceManager.hpp"
#include "LocalPlayer.hpp"
#include <iostream>

World world("world");

GameManager::GameManager()
{
	// TODO Auto-generated constructor stub

}

GameManager::~GameManager()
{
	// TODO Auto-generated destructor stub
}

// Start the Game and process any command line args
void GameManager::start(int argc, char* argv[])
{
	ResourceManager::init();
	return run();
}

void GameManager::update()
{
	world.update();
}


void GameManager::draw()
{
	static bool hasRun = false;
	if(!hasRun)
	{
		world.loadChunk(0);
		world.loadChunk(1);
		world.spawnEntity(EntityType::LocalPlayer, Location(sf::Vector2f(10.0f, 10.0f), 0));
		//world.loadChunk(-1);
		//world.unloadChunk(-1);
		hasRun = true;
	}



	WindowManager::getWindow().clear(sf::Color::Black);
	world.draw();
	WindowManager::getWindow().display();
}

// Main Game Loop
void GameManager::run()
{
	WindowManager::init(600, 400);
	//WindowManager::getWindow().setFramerateLimit(1000);

	sf::Clock tickClock, fpsClock;
	unsigned short fps = 0;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	while (WindowManager::isOpen())
	{
		timeSinceLastUpdate = tickClock.getElapsedTime();
		if (timeSinceLastUpdate > sf::milliseconds(50))
		{
			WindowManager::pollEvents();
			update();
			//std::cout << "Update!" << std::endl;
			//numOfUpdates++;
			timeSinceLastUpdate = sf::Time::Zero;
			tickClock.restart();
		}

		draw();
		fps++;


		if (fpsClock.getElapsedTime() > sf::seconds(1))
		{
			fpsClock.restart();
			std::cout << "FPS: " << fps << std::endl;
			fps = 0;
		}
	}
}
