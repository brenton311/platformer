/*
 * WindowManager.cpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#include "WindowManager.hpp"

// Declare the static members
sf::RenderWindow WindowManager::s_window;
bool WindowManager::s_active;

void WindowManager::init(unsigned short width, unsigned short height)
{
	s_window.create(sf::VideoMode(width, height), "Platformer");
	s_window.setFramerateLimit(200);
	s_active = true;
}

bool WindowManager::isOpen()
{
	return s_window.isOpen();
}

bool WindowManager::isActive()
{
	return s_active;
}

sf::Vector2u WindowManager::getSize()
{
	return s_window.getSize();
}

void WindowManager::pollEvents()
{
	static sf::Vector2f loc(0.0f, 0.0f);
	sf::Event event;

	while (s_window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			s_window.close();
		else if (event.type == sf::Event::LostFocus)
			s_active = false;
		else if (event.type == sf::Event::GainedFocus)
			s_active = true;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && isActive())
		s_window.close();
}

sf::RenderWindow& WindowManager::getWindow()
{
	return s_window;
}
