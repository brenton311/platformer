/*
 * Exceptions.cpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#include "Exceptions.hpp"


/*
 * Base Exception Class
 */
Exception::Exception(const std::string& msg)
: m_errorMsg(msg)
{}

Exception::~Exception()
{}

const std::string& Exception::getErrorMsg() const
{
	return m_errorMsg;
}

