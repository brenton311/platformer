/*
 * Chunk.cpp
 *
 *  Created on: Jan 15, 2014
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include "Chunk.hpp"
#include "LocalPlayer.hpp"
#include "Location.hpp"
#include "Exceptions.hpp"
#include "WindowManager.hpp"
#include "ResourceManager.hpp"
#include <SFML/Graphics/RenderTexture.hpp>

sf::RenderTexture target;

Chunk::Chunk(int pos, const std::string& worldName) :
		m_vertices(sf::PrimitiveType::Quads), m_loc(pos), m_worldDir(worldName)
{

	target.create(WindowManager::getSize().x, WindowManager::getSize().y,
			false);

	// By default all of the tiles should be Air
	for (int x = 0; x < CHUNK_TILE_WIDTH; x++)
	{
		for (int y = 0; y < CHUNK_TILE_HEIGHT; y++)
			m_tiles[x][y] = TileID::Air;
	}

}

Chunk::~Chunk()
{
	std::cout << "Unloading Chunk: " << m_loc << "!" << std::endl;

	for (Entity* e : m_entitys)
		delete e;
}

void Chunk::spawnEntity(EntityType eType, Location loc, unsigned int id)
{
	try
	{
		switch (eType)
		{
		case EntityType::LocalPlayer:
		{
			m_entitys.push_back(
					new LocalPlayer(Location(sf::Vector2f(10.0f, 10.0f), 0),
							0));
			break;
		}
		case EntityType::Player:
			// TODO
			break;
		}

	} catch (...)
	{
		//		delete e;
		throw;
	}/*
	 std::cout << "Entity Spawned @ " << m_loc << " -> Type: "
	 << m_entitys.at(m_entitys.size() - 1)->getID() << " ID: "
	 << m_entitys.at(m_entitys.size() - 1)->getID() << std::endl;*/
}

std::vector<Entity*> Chunk::getEntitys()
{
	return m_entitys;
}

void Chunk::update()
{
	for (Entity* e : m_entitys)
		e->update();
}

void Chunk::load()
{
	// Find the resulting file path
	std::ifstream file;
	std::stringstream path;
	path << m_worldDir << "/" << m_loc;
	std::cout << "Loading chunk file: " << path.str() << std::endl;
	file.open(path.str().c_str());

	if (!file.is_open())
		throw Exception("Failed to load Chunk: " + path.str());

	// Loads the entire file
	std::stringstream buffer;
	buffer << file.rdbuf();
	file.close();

	// Load all the tiles
	for (int y = 0; y < CHUNK_TILE_HEIGHT; y++)
	{
		if (!buffer.good())
			throw Exception("Error loading Chunk: " + path.str() + "!");

		// Load the line into the buffer
		std::string line;
		std::getline(buffer, line);
		std::istringstream iStream(line);

		// Get the 16 tiles across on the x-axis
		for (int x = 0; x < CHUNK_TILE_WIDTH; x++)
		{
			int id;
			iStream >> id;
			m_tiles[x][y] = TileUtl::parseID(static_cast<unsigned short>(id));
		}
		//std::cout << "\n";
	}

	// Build the vertex array

	// The chunks offset from the (0, 0) position. Only apply's to x-axis
	sf::Vector2f chunkOffset = sf::Vector2f(
			m_loc * CHUNK_TILE_WIDTH * TILE_SIZE, 0.0f);

	for (int y = 0; y <= (CHUNK_TILE_HEIGHT / 1); y++)
	{
		for (int x = 0; x <= (CHUNK_TILE_WIDTH / 1); x++)
		{
			sf::Vertex quad[4];

			// Set the color for all of the vertices
			for (int i = 0; i < 4; i++)
				quad[i].color = sf::Color::Red;

			sf::Vector2f offset = sf::Vector2f(x * 8, y * 8); //+ chunkOffset;
			offset += chunkOffset;

			quad[0].position = offset + sf::Vector2f(0.0f, 0.0f);
			quad[1].position = offset + sf::Vector2f(TILE_SIZE, 0.0f);
			quad[2].position = offset + sf::Vector2f(TILE_SIZE, TILE_SIZE);
			quad[3].position = offset + sf::Vector2f(0.0f, TILE_SIZE);

			for (int i = 0; i < 4; i++)
				m_vertices.append(quad[i]);
		}
	}
}

void Chunk::draw()
{
	WindowManager::getWindow().draw(m_vertices);

	//	 Draw all of the entity's
	for (Entity* e : m_entitys)
		e->draw();
}

int Chunk::getLocation() const
{
	return m_loc;
}
