/*
 * LocalPlayer.cpp
 *
 *  Created on: Jan 24, 2014
 */

#include "LocalPlayer.hpp"
#include "WindowManager.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
#include <iostream>

LocalPlayer::LocalPlayer(Location loc, unsigned short id) :
		Entity(EntityType::LocalPlayer, loc, id)

{
	std::cout << "Spawning Local Player!" << std::endl;
}

void LocalPlayer::draw()
{
	WindowManager::getWindow().draw(m_sprite);
}

LocalPlayer::~LocalPlayer()
{
	std::cout << "Despawning Local Player!" << std::endl;
}

void LocalPlayer::update()
{
	//std::cout << "Local Player Update!" << std::endl;
	//sf::Vector2f currentPos = m_sprite.getPosition();

	sf::View view = WindowManager::getWindow().getView();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && WindowManager::isActive())
		view.move(-4.0f, 0.0f);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) &&  WindowManager::isActive())
		view.move(4.0f, 0.0f);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) &&  WindowManager::isActive())
		view.move(0.0f, -4.0f);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) &&  WindowManager::isActive())
		view.move(0.0f, 4.0f);

	WindowManager::getWindow().setView(view);
}
