/*
 * Start.cpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#include <iostream>
#include <exception>
#include "Exceptions.hpp"
#include "GameManager.hpp"

int main(int argc, char* argv[])
{
	try
	{
		GameManager gm;
		gm.start(argc, argv);
	}
	catch (Exception& e)
	{
#ifdef DEBUG
		std::cout << "Error: " << e.getErrorMsg() << std::endl;
#endif
	}
	catch(std::exception& e)
	{
#ifdef DEBUG
		std::cout << "STD Exception: " << e.what() << std::endl;
#endif
	}
	catch (...)
	{
#ifdef DEBUG
		std::cout << "Unknown Error!" << std::endl;
#endif
	}
}

