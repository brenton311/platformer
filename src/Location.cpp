/*
 * Location.cpp
 *
 *  Created on: Jan 21, 2014
 */

#include "Location.hpp"

Location::Location(sf::Vector2f worldLoc, int chunkLoc) :
		m_loc(worldLoc), m_chunkLoc(chunkLoc)
{}

sf::Vector2f Location::getLoc() const
{
	return m_loc;
}

const int Location::getChunkLoc() const
{
	return m_chunkLoc;
}
