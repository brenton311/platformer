/*
 * Entity.hpp
 *
 *  Created on: Jan 21, 2014
 */

#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <SFML/Graphics/Sprite.hpp>
#include "DrawableEntity.hpp"

class Location;

/*
 * Entity Base Class intended to
 * inherited my all Entity's
 * */
class Entity: public DrawableEntity
{
public:
	Entity(EntityType eType, Location loc, unsigned short id);
	virtual ~Entity();
	virtual void update();
	virtual void draw();
	unsigned short getID();
protected:
	const unsigned short m_id;

};

#endif /* ENTITY_HPP */
