/*
 * Location.hpp
 *
 *  Created on: Jan 21, 2014
 */

#ifndef LOCATION_HPP
#define LOCATION_HPP

#include <SFML/System/Vector2.hpp>
#include <string>

class Location
{
public:
	Location(sf::Vector2f worldLoc, int chunkLoc);
	sf::Vector2f getLoc() const;
	const int getChunkLoc() const;
private:
	// The location in screen coords
	const sf::Vector2f m_loc;

	// The chunk at the screen coords
	const int m_chunkLoc;
};

#endif /* LOCATION_HPP */
