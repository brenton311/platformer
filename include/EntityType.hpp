/*
 * EntityType.hpp
 *
 *  Created on: Jan 23, 2014
 */

#ifndef ENTITYTYPE_HPP
#define ENTITYTYPE_HPP

// For keeping track of the Entity's Type
enum class EntityType
{
	Player,
	LocalPlayer
};

#endif /* ENTITYTYPE_HPP */
