/*
 * ResourceManager.hpp
 *
 *  Created on: Jan 22, 2014
 */

#ifndef RESOURCEMANAGER_HPP
#define RESOURCEMANAGER_HPP

#include <map>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Audio/Sound.hpp>


enum class TextureID
{
	PlayerSprite,
	StoneTile,
	GrassTile
};


class ResourceManager
{
public:
	static void init();
	static sf::Texture& getTexture(TextureID id);
private:
	static void loadTexture(const std::string& path, TextureID id);
	static std::map<TextureID, sf::Texture*> s_resources;

};

#endif /* RESOURCEMANAGER_HPP */
