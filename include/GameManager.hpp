/*
* GameManager.hpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

class GameManager
{
public:
	GameManager();
	~GameManager();
	void start(int argc, char* argv[]);
private:
	void run();
	void draw();
	void update();
};

#endif /* GAMEMANAGER_HPP */
