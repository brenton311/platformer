/*
 * DrawableEntity.hpp
 *
 *  Created on: Jan 23, 2014
 */

#ifndef DRAWABLEENTITY_HPP
#define DRAWABLEENTITY_HPP

#include "Location.hpp"
#include "EntityType.hpp"
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Sprite.hpp>

class DrawableEntity: sf::NonCopyable
{
public:
	DrawableEntity(EntityType eType, Location loc);
	virtual ~DrawableEntity(){}
	virtual void draw();
	Location getLocation();
	const EntityType type;
protected:
	sf::Sprite m_sprite;
	Location m_loc;
};

#endif /* DRAWABLEENTITY_HPP */
