/*
 * World.hpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#ifndef WORLD_HPP
#define WORLD_HPP

#include <string>
#include <SFML/Graphics.hpp>
#include "EntityType.hpp"

class Location;
class Chunk;

class World: sf::NonCopyable
{
public:
	World(const std::string& name);
	~World();
	const std::string& getName();
	void unloadChunk(int loc);
	void loadChunk(int loc);
	Chunk* getChunk(int loc);
	std::vector<Chunk*>& getLoadedChunks();
	void update();
	void draw();
	void spawnEntity(EntityType eType, Location loc);
private:
	unsigned short m_nextID;
	unsigned short genEntityID();
	std::string m_name;
	sf::RenderTexture m_mapTexture;
	std::vector<Chunk*> m_loadedChunks;
};

#endif /* WORLD_HPP */
