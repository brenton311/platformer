/*
 * Chunk.hpp
 *
 *  Created on: Jan 15, 2014
 */

#ifndef CHUNK_HPP
#define CHUNK_HPP

#include "TileUtl.hpp"
#include "EntityType.hpp"
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <vector>

class Location;
class Entity;

// The number of tiles in a chunk
const unsigned short CHUNK_TILE_WIDTH = 16;
const unsigned short CHUNK_TILE_HEIGHT = 64;

class Chunk : sf::NonCopyable
{
public:
	Chunk(int pos, const std::string& worldName);
	~Chunk();
	void update();
	void load();
	int getLocation() const;
	void spawnEntity(EntityType eType, Location loc, unsigned int id);
	std::vector<Entity*> getEntitys();
	//const sf::Sprite& getSprite() const;
	void draw();
private:
	sf::VertexArray m_vertices;
	std::vector<Entity*> m_entitys;
	TileID m_tiles[16][64];
	int m_loc;
	std::string m_worldDir;
};

#endif /* CHUNK_HPP */
