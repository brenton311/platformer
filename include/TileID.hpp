/*
 * TileID.hpp
 *
 *  Created on: Jan 19, 2014
 *
 */

#ifndef TILEID_HPP
#define TILEID_HPP

// Width and height in pixels
const unsigned short TILE_SIZE = 8;

// ID's for all the tiles in the game
enum TileID
{
	// Normal Back Tiles
	Air,
	Stone,
	Grass,
	Dirt,
	Wood,

	// Front Tiles
	FrontWood,
};

#endif /* TILEID_HPP */
