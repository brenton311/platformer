/*
 * LocalPlayer.hpp
 *
 *  Created on: Jan 24, 2014
 */

#ifndef LOCALPLAYER_HPP
#define LOCALPLAYER_HPP

#include "Entity.hpp"

class LocalPlayer : public Entity
{
public:
	LocalPlayer(Location loc, unsigned short id);
	virtual ~LocalPlayer();
	virtual void update();
	virtual void draw();
protected:
};

#endif /* LOCALPLAYER_HPP */
