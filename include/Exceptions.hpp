/*
 * Exceptions.hpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <exception>
#include <string>



// Base Class for an exception
class Exception
{
public:
	Exception(const std::string& msg);
	virtual ~Exception();
	virtual const std::string& getErrorMsg() const;
private:
	std::string m_errorMsg;
};


/*
 * For When a file failed to load or is missing
 */
class FileLoadingException : public Exception
{
public:
	FileLoadingException(const std::string& msg);
	virtual ~FileLoadingException();
	virtual const char* what() const;
private:
	std::string m_errorMsg;
};

#endif /* EXCEPTIONS_HPP */
