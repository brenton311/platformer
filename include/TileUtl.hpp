/*
 * TileUtl.hpp
 *
 *  Created on: Jan 19, 2014
 *
 */

#ifndef TILEUTL_HPP
#define TILEUTL_HPP

#include "TileID.hpp"

class TileUtl
{
public:
	static TileID parseID(unsigned short id);
	static bool isBackTile(TileID id);
};

#endif /* TILEUTL_HPP */
