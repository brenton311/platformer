/*
 * WindowManager.hpp
 *
 *  Created on: Jan 15, 2014
 *
 */

#ifndef WINDOWMANAGER_HPP
#define WINDOWMANAGER_HPP

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/System/Vector2.hpp>

class WindowManager
{
public:
	static void init(unsigned short width, unsigned short height);
	static sf::Vector2u getSize();
	static void pollEvents();
	static bool isOpen();
	static bool isActive();
	static sf::RenderWindow& getWindow();
private:
	WindowManager();
	static sf::RenderWindow s_window;
	static bool s_active;

};

#endif /* WINDOWMANAGER_HPP_ */
